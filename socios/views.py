from django.shortcuts import render, reverse, HttpResponseRedirect
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from socios.helpers import *
# import time
import os

# Create your views here.


def principal(request):
    if request.method == 'POST':
        if 'document' in request.FILES:
            archivo_subido = request.FILES['document']
            if not archivo_subido.name.endswith('.csv'):
                messages.error(request, 'El archivo subido no es un CSV')

            fs = FileSystemStorage()
            if fs.exists(archivo_subido.name):
                os.remove(os.path.join(settings.MEDIA_ROOT, archivo_subido.name))
            fs.save(archivo_subido.name, archivo_subido)
            return HttpResponseRedirect(reverse('socios:informacion',
                                        kwargs={'slug': archivo_subido.name}))
    return render(request, 'socios/principal.html')


def informacion(request, slug):
    # start_time = time.time()
    filtros_queries = importar_socios(slug)

    data = {'cant_registrados': filtros_queries['cant_registrados'],
            'promedio_edad_socios_racing':
            calcular_promedio_edad_socios_club(
                filtros_queries['hinchas_racing']),
            'universitarios_casados':
            listar_cien_socios_edad_descendiente(
                filtros_queries['casados_universitarios']),
            'nombres_comunes_river':
            listar_cinco_nombres_comunes_socios(
                filtros_queries['hinchas_river']),
            'edades_equipos':
            listar_equipos_edad_promedio_min_y_max(
                filtros_queries['total_socios'])
            }
    # print("--- %s seconds ---" % round((time.time() - start_time),5))

    return render(request, 'socios/informacion.html', data)
