from django.urls import path

from . import views

app_name = 'socios'

urlpatterns = [
    path('', views.principal, name='principal'),
    path('informacion/<str:slug>/', views.informacion, name='informacion'),
    ]
