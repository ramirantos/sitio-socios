from django.conf import settings

import csv
import os
import pandas as pd


def importar_socios(nombre_archivo):
    path_archivo = os.path.join(settings.MEDIA_ROOT, nombre_archivo)
    filtro_para_queries = {'cant_registrados': 0,
                           'hinchas_racing': [],
                           'casados_universitarios': [],
                           'hinchas_river': [],
                           'total_socios': [], }

    with open(path_archivo) as archivo_csv:
        socios_reader = csv.reader(archivo_csv, delimiter=',')
        for fila in socios_reader:
            filtro_para_queries['cant_registrados'] += 1
            fila[1] = int(fila[1])

            if fila[2].lower() == "racing":
                filtro_para_queries['hinchas_racing'].append(fila)
            if "casad" in fila[3].lower() and "universitario" in fila[4].lower():
                filtro_para_queries['casados_universitarios'].append(fila)
            if fila[2].lower() == "river":
                filtro_para_queries['hinchas_river'].append(fila)

            filtro_para_queries['total_socios'].append(fila)

    return filtro_para_queries


def calcular_promedio_edad_socios_club(socios):
    edades_acumuladas = 0
    total_socios = 0

    for socio in socios:
        edades_acumuladas += socio[1]
        total_socios += 1

    return round(edades_acumuladas / total_socios, 2)


def listar_cien_socios_edad_descendiente(socios):
    socios.sort(key=lambda x: x[1])

    socios = socios[:100]
    resultado = []
    for socio in socios:
        resultado.append(socio[:3])

    return resultado


def listar_cinco_nombres_comunes_socios(socios):

    aparicion_nombres = {}

    for socio in socios:
        if not socio[0] in aparicion_nombres:
            aparicion_nombres[socio[0]] = 0
        aparicion_nombres[socio[0]] += 1

    aparicion_nombres_ordenados = []
    for clave in aparicion_nombres:
        aparicion_nombres_ordenados.append([clave, aparicion_nombres[clave]])

    aparicion_nombres_ordenados.sort(key=lambda x: x[1], reverse=True)

    resultado = []

    for elem in aparicion_nombres_ordenados[:5]:
        resultado.append(elem[0])

    return resultado


def listar_equipos_edad_promedio_min_y_max(socios):
    # cada clave del diccionario tendra como valor una lista con
    # estructura: [total_socios, edad_acumulada, edad_min, edad_max]
    socios_por_equipo = {}

    for socio in socios:
        if not socio[2] in socios_por_equipo:
            socios_por_equipo[socio[2]] = [1, socio[1], socio[1], socio[1]]
        else:
            socios_por_equipo[socio[2]][0] += 1
            socios_por_equipo[socio[2]][1] += socio[1]

            if socio[1] < socios_por_equipo[socio[2]][2]:
                socios_por_equipo[socio[2]][2] = socio[1]
            elif socio[1] > socios_por_equipo[socio[2]][3]:
                socios_por_equipo[socio[2]][3] = socio[1]

    resultado = []

    for equipo in socios_por_equipo:
        data = [equipo]
        data += [round(socios_por_equipo[equipo][1] / socios_por_equipo[equipo][0], 2)]
        data += socios_por_equipo[equipo][2:] + [socios_por_equipo[equipo][0]]
        resultado.append(data)

    resultado.sort(key=lambda x: x[4], reverse=True)

    for equipo in resultado:
        equipo.remove(equipo[4])

    return resultado
